from app import app
from flask import jsonify

# Home index endpoint 
@app.route('/')
@app.route('/index')
def index():
    return "Hello, World!"

# Dummy route test - gidi 
@app.route('/gidi')
def gidi():
    return "Hello, Gidi!"

# Dummy about - route tests     
@app.route('/about')
def about():
    return "About python flask virtual env!"

# Return dummy json data - json endpoint test 
@app.route('/json')
@app.route('/json/')
def json_response():
    response = {'status': '01', 'description': 'Json response.', 'data':'This is some other dummy data....'}
    return jsonify(response)